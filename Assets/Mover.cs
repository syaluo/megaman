﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {
    public GameObject rightBullet;
    public GameObject leftBullet;
    private Rigidbody rb;
    private int numJumps;
    private float timer = 0;
    public float velocityMax;
    public float fx;
    public float jumpVel;
    public bool isCollided;
    private bool direction;
    private bool doubleJump;
    public AudioSource jump;
    public AudioSource death;
    public Canvas can;
    private Animator anim;
    private Transform t;


    // Use this for initialization
    void Start() {
        Physics.gravity = new Vector3(0, -20, 0);
        rb = GetComponent<Rigidbody>();
        anim = GameObject.Find("fire_mario").GetComponent<Animator>();
        t = GameObject.Find("fire_mario").GetComponent<Transform>();
        anim.SetInteger("Stand", 1);

    }
    public void die()
    {
        can.enabled = true;
        Time.timeScale = 0;
    }
	// Update is called once per frame
	void FixedUpdate () {
        if (transform.position.y < -25)
        {
            
            die();
        }
        timer += Time.deltaTime;

        if (Input.GetKeyDown("up") && canPlayerJump())
        {
            jump.Play();
            rb.velocity = new Vector3(rb.velocity.x, jumpVel, rb.velocity.z);
            isCollided = false;
            numJumps++;
        }
        if (Input.GetKey("left"))
        {
            if(rb.velocity.x >= -velocityMax)
            {
                rb.AddForce(new Vector3(-fx, 0, 0));
                t.localScale = new Vector3((float)5.66, t.localScale.y, t.localScale.z);
                anim.SetInteger("Stand", 1);

            }
            direction = false;
        }
        if (Input.GetKey("right"))
        {
            if (rb.velocity.x <= velocityMax)
            {
                rb.AddForce(new Vector3(fx, 0, 0));
                t.localScale = new Vector3((float)-5.66, t.localScale.y, t.localScale.z);
                anim.SetInteger("Stand", 1);
            }
            direction = true;
        }
        if (!Input.GetKey("right") && !Input.GetKey("left"))
        {
            if(rb.velocity.x > -.5 && rb.velocity.x < .5)
            {
                rb.velocity = new Vector3(0, rb.velocity.y, rb.velocity.z);
            }
            else if(rb.velocity.x >= .5)
            {
                rb.AddForce(-fx, 0, 0);
            }
            else
            {
                rb.AddForce(fx, 0, 0);
            }
        }
        if (Input.GetKey("space"))
        {
            if (timer > 0.5)
            {

                if(direction == true)
                {
                    Instantiate(rightBullet, this.gameObject.transform.position + new Vector3((float)1.0, 0, 0), Quaternion.identity);
                    timer = 0;
                }
                else
                {
                    Instantiate(leftBullet, this.gameObject.transform.position + new Vector3((float)-1.0, 0, 0), Quaternion.identity);
                    timer = 0;
                }
                
            }
        }
    }
    private bool canPlayerJump()
    {
        if (isCollided)
        {
            numJumps = 0;
            return true;
        }
        else
        {
            if(numJumps < 2)
            {
                print("should jump");
                return true;
            }
            else
            {
                print("shouldnt jump");
                return false;
            }
        }
    }
}
