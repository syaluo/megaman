﻿using UnityEngine;
using System.Collections;


public class BulletTime : MonoBehaviour {
    private float time;
    private AudioSource aud;
	// Use this for initialization
	void Start () {
        aud = GameObject.Find("Platform").GetComponent<AudioSource>();
	}
    void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Villain")
        {

            aud.Play();
            Destroy(other.gameObject);
            Destroy(this.gameObject); }
      
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Boss")
        {
            other.GetComponent<FlyingEnemy>().lives -= 1;
            Destroy(this.gameObject);
        }
        if (other.tag == "Superboss")
        {
            other.GetComponent<FlyingEnemy>().lives -= 1;
            aud.Play();
            Destroy(this.gameObject);
      
        }
    }
    // Update is called once per frame
    void FixedUpdate () {
        time += Time.deltaTime;
        if (time > 2)
         { Destroy(this.gameObject); }
	}
}
