﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
    public bool moving;
    private Rigidbody rig;
    public float timerstart;
    public float velocity;
    public float period;

	// Use this for initialization
	void Start () {
        rig = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        timerstart += Time.deltaTime;
	    if (moving == true)
        {
            if (timerstart % period > period/2)
            {
                rig.velocity = new Vector3(0, velocity, 0);
            }
            else
            {
                rig.velocity = new Vector3(0, -velocity, 0);
            }
        }
	}
}
