﻿using UnityEngine;
using System.Collections;

public class EnemyBulletFly : MonoBehaviour
{
    private Rigidbody rig;
    private float timer;
    public float bulletspeed;
    // Use this for initialization
    void Start()
    {
        rig = GetComponent<Rigidbody>();
        rig.velocity = new Vector3(-bulletspeed, 0, 0);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Platform")
        {
            Destroy(this.gameObject); 
        }
    }
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > 3.0)
        {
            Destroy(this.gameObject);
        }
    }
}
