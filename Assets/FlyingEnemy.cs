﻿using UnityEngine;
using System.Collections;

public class FlyingEnemy : MonoBehaviour {
    private float time;
    private Rigidbody rig;
    public float speed;
    public float period;
    public bool direction = true;
    public bool fire;
    public GameObject bullet;
    public float firerate;
    private float timer2;
    public bool boss;
    public bool Superboss;
    public int lives = -1;
	// Use this for initialization
	void Start () {
        rig = GetComponent<Rigidbody>();
        if (boss == true)
        {
            this.gameObject.tag = "Boss";
        }
        if (Superboss == true)
        {
            this.gameObject.tag = "Superboss";
        }
    }
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        timer2 += Time.deltaTime;
        if (lives == 0)
            {
            Destroy(this.gameObject);
        }
        if (lives == 0 && this.gameObject.tag == "Superboss")
        {
            Destroy(this.gameObject);
            this.gameObject.GetComponent<WinGame>().wg();
         
        }

	}
    void FixedUpdate()
    {
        if (direction == true)
        {
            if (time % period > period / 2)
            {
                rig.velocity = new Vector3(0, speed, 0);
            }
            else
            {
                rig.velocity = new Vector3(0, -speed, 0);
            }
        }
        else
        {
            if (time % period > period / 2)
            {
                rig.velocity = new Vector3(speed, 0, 0);
            }
            else
            {
                rig.velocity = new Vector3(-speed, 0, 0);
            }
        }
        if (fire == true && timer2 > firerate)
        {
            Instantiate(bullet, transform.position + new Vector3(-2, 0, 0), Quaternion.identity);
            timer2 = 0;
        }

    }
}
