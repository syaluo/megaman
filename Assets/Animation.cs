﻿using UnityEngine;
using System.Collections;

public class Animation : MonoBehaviour {
    public float width = 10;
    public float height = 10;
    public Vector3 position = new Vector3(10, 5, 0);
    // Use this for initialization
    void Start () {
	
	}

    void Awake()
    {
        // set the scaling
        Vector3 scale = new Vector3(width, height, 1f);
        transform.localScale = scale;
        // set the position
        transform.position = position;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
