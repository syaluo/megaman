﻿using UnityEngine;
using System.Collections;

public class jumper : MonoBehaviour {
    private AudioSource aud;
    public GameObject character;

   
	// Use this for initialization
	void Start () {
        aud = GetComponent<AudioSource>();
	}
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Platform")
        {
            
            GameObject.Find("Character").GetComponent<Mover>().isCollided = true;

            
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Villain")
        {

            aud.Play();
            GameObject.Find("Character").GetComponent<Mover>().die();
            
        }
    }
    // Update is called once per frame
    void Update () {
	
	}
}
